package com.dsa;

import java.util.ArrayList;
import java.util.List;

public class LongestWordsInListOfStrings {
    public static void main(String[] args) {
        String dictionary[] = {"to", "toe", "toes", "doe", "dog", "god", "dogs", "book", "banana"};
        longestWords(dictionary, "oetdg");
    }

    public static void longestWords(String arr[], String letters) {
        List<String> res = new ArrayList<>();
        Boolean flag = true;
        int maxLength = 0;
        for (String val : arr) {
            for (Character ch : val.toCharArray()) {
                if (!letters.contains(ch.toString())) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                maxLength = Math.max(maxLength, val.length());
                res.add(val);
            }
            flag = true;
        }
        int finalMaxLength = maxLength;
        res.stream().filter(a -> a.length() >= finalMaxLength).forEach(System.out::println);
    }
}
