package com.dsa;

public class FIndThePowerOfX {
    public static void main(String[] args) {
        System.out.println(pow(2, 4));
    }
    public static int pow(int a, int n) {
        if (n == 0)
            return 1;
        if (n == 1)
            return a;
        else
            return a * pow(a, n - 1);
    }
}
