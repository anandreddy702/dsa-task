package com.dsa;

public class MinMaxCharSeq {
    public static void main(String[] args) {
        maxSeq();
    }

    public static void maxSeq() {
        String str = "saassbcdmmmmmmmmmmefaaaaaaaaaaaaaaaertaxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        int n = str.length();
        int count = 0;
        char res = str.charAt(0);
        int cur_count = 1;
        int res_start_index = 0;
        for (int i = 0; i < n; i++) {
            if (i < n - 1 && str.charAt(i) == str.charAt(i + 1)) {
                cur_count++;
            } else {
                if (cur_count > count) {
                    count = cur_count;
                    res = str.charAt(i);
                    res_start_index = i - count + 1;
                }
                cur_count = 1;
            }
        }
        System.out.println(res);
        System.out.println(count);
        System.out.println("Start Index -> " + res_start_index);
    }
}
