package com.dsa;

public class DotProduct {
    public static void main(String[] args) {
        int arr1[] = {1, 2};
        int arr2[] = {2, 3};
        dotProduct(arr1, arr2);
    }
    private static void dotProduct(int[] arr1, int[] arr2) {
        int n = arr1.length;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr1[i] * arr2[i];
        }
        System.out.println(sum);
    }
}
