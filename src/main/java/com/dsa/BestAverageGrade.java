package com.dsa;

import java.util.HashMap;
import java.util.Map;

public class BestAverageGrade {

    public static void main(String[] args) {
        String input[][] = {{"Bobby", "92"}, {"Charles", "100"}, {"Eric", "64"}, {"Charles", "22"}};
        System.out.println(findMaxAverage(input));
    }
    public static int findMaxAverage(String[][] scores) {
        if (scores.length == 0) return -1;
        Map<String, Integer> noOfSubjects = new HashMap<>();
        Map<String, Integer> totalMarks = new HashMap<>();
        for (int i = 0; i < scores.length; i++) {
            noOfSubjects.put(scores[i][0], noOfSubjects.getOrDefault(scores[i][0], 0) + 1);
            totalMarks.put(scores[i][0], totalMarks.getOrDefault(scores[i][0], 0) + Integer.parseInt(scores[i][1]));
        }
        int max = 0;
        for (String student : totalMarks.keySet()) {
            max = Math.max(max, totalMarks.get(student) / noOfSubjects.get(student));
        }
        return max;
    }

    static class Student {
        String name;
        int mark;
        int count;

        public Student(String name, int mark, int count) {
            this.name = name;
            this.mark = mark;
            this.count = count;
        }
    }


}
