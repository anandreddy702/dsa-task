package com.dsa;

import java.util.HashSet;

public class UniqueSubstringsOfGivenLength {

    public static void main(String[] args) {
        HashSet<String> abxccde = getUniqueSubStrings("abcdeefghijk", 3);
        abxccde.forEach(System.out::println);
    }
    public static HashSet<String> getUniqueSubStrings(String s, int size) {
        HashSet<String> res = new HashSet<>();
        String currString = "";
        for (Character ch : s.toCharArray()) {
            if (currString.length() == size) {
                res.add(currString);
                currString = currString.substring(1);
            }
            if (!currString.contains(String.valueOf(ch))) {
                currString = currString + ch;
            } else {
                currString = String.valueOf(ch);
            }
        }
        if(currString.length() == size) res.add(currString);
        return res;
    }
}
