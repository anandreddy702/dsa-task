package com.dsa;

import java.util.Arrays;

public class LengthOfLongestPathInBinaryTree {
    public static void main(String[] args) {
        Node node = new Node(10);
        node.left = new Node(20);
        node.right = new Node(30);
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.left = new Node(60);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);
        int maxDepth = getMaxDepth(node);
        System.out.println(1+maxDepth);
    }
    public static int getMaxDepth(Node root) {
        if(root == null) return -1;
        int lDepth = getMaxDepth(root.left);
        int rDepth = getMaxDepth(root.right);
        return 1 + Math.max(lDepth, rDepth);
    }
}
