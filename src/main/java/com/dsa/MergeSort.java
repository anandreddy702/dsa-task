package com.dsa;

import java.util.Arrays;

public class MergeSort {
    public static void main(String[] args) {
        int arr[] = { 12, 11, 13, 5, 6, 7, 1 };
        devide(arr, 0, arr.length-1);
        System.out.println(Arrays.toString(arr));
    }
    public static void devide(int arr[], int left, int right){
        if(left < right){
            int middle = (left + (right-1))/2;
            devide(arr, left, middle);
            devide(arr, middle+1, right);
            merge(arr, left, middle, right);
        }
    }
    public static void merge(int arr[], int left, int middle, int right){
        int n1 = middle-left+1;
        int n2 = right - middle;
        int temp1[] = new int[n1];
        int temp2[] = new int[n2];
        for(int i = 0; i < n1; i++)
            temp1[i] = arr[left+i];
        for(int i = 0; i < n2; i++)
            temp2[i] = arr[i+middle+1];
        int i = 0, j = 0, k = left;
        while(i < n1 && j < n2){
            if(temp1[i] <= temp2[j]){
                arr[k] = temp1[i];
                i++;
            }else{
                arr[k] = temp2[j];
                j++;
            }
            k++;
        }
        while(i < n1){
            arr[k] = temp1[i];
            i++; k++;
        }
        while(j < n2){
            arr[k] = temp2[j];
            j++;k++;
        }
    }

}
