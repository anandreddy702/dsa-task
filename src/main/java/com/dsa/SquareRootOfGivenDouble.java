package com.dsa;

public class SquareRootOfGivenDouble {
    public static void main(String[] args) {
        System.out.println(squareRoot(20));
    }
    public static double squareRoot(int number) {
        int start = 0, mid = 0, end = number;
        double ans = 0.0;
        while (start <= end) {
            mid = (start + end) / 2;
            if (mid * mid == number) {
                return mid;
            }
            if (mid * mid < number) {
                start = mid + 1;
                ans = mid;
            } else {
                end = mid - 1;
            }
        }
        return ans;
    }

}