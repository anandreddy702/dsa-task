package com.dsa;

import java.util.HashMap;
import java.util.Map;

public class IpAddresses {
    public static void main(String[] args) {
        String lines[] = new String[]{
                "10.0.0.1 - frank [10/Dec/2000:12:34:56 -0500] \"GET /a.gif HTTP/1.0\" 200 234",
                "10.0.0.1 - frank [10/Dec/2000:12:34:57 -0500] \"GET /b.gif HTTP/1.0\" 200 234",
                "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
                "10.0.0.2 - nancy [10/Dec/2000:12:34:59 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
                "10.0.0.3 - logan [10/Dec/2000:12:34:59 -0500] \"GET /d.gif HTTP/1.0\" 200 234",};
        ipaddress(lines);
    }

    public static void ipaddress(String lines[]) {
        HashMap<String, Integer> map = new HashMap();
        int maxCount = 1;
        for (String str : lines) {
            if (map.containsKey(str.substring(0, 8))) {
                map.put(str.substring(0, 8), map.get(str.substring(0, 8)) + 1);
                maxCount = Math.max(maxCount, map.get(str.substring(0, 8)));
            } else
                map.put(str.substring(0, 8), 1);
        }
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == maxCount) {
                System.out.println(entry.getKey());
            }
        }
    }
}
