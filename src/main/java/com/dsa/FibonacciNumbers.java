package com.dsa;

public class FibonacciNumbers {
	public static void main(String[] args) {
		fibonacci(10);
	}
	
	public static void fibonacci(int n) {
		int pre = 0;
		int current = 1;
		int sum = 0;
		for(int i = 0; i < n; i++) {
			sum = pre + current;
			System.out.print(sum + " ");
			pre = current;
			current = sum;
		}
	}
}
