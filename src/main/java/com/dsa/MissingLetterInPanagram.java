package com.dsa;

public class MissingLetterInPanagram {

    public static void main(String[] args) {
        missingLetterInPanagram("The slow purple oryx meanders past the quiescent canine");
    }

    private static void missingLetterInPanagram(String str) {
        String s = str.toLowerCase();
        int arr[] = new int[256];
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i)]++;
        }
        for (int i = 0; i < 256; i++) {
            if (arr[i] == 0 && i >= 97 && i <= 122) {
                System.out.print((char) (i));
            }
        }
    }
}
