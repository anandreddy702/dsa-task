package com.dsa;

public class IsPowerOfX {

    public static void main(String[] args) {
        System.out.println(isPowerOf(16, 2));
    }
    private static boolean isPowerOf(int a, int n) {
        int res = n;
        while(res <= a){
            if(res == a) return true;
            res = res * n;
        }
        return false;
    }
}
