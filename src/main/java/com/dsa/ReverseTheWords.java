package com.dsa;

public class ReverseTheWords {

	public static void main(String[] args) {
		String str = "Hi mama how are u";
		reverseTheWords(str);
	}

	public static void reverseTheWords(String str) {
		String[] split = str.split(" ");
		String finalString = "";
		for (String a : split) {
			String reverseTheString = reverseTheString(a);
			finalString = finalString + " " + reverseTheString;
		}
		System.out.println(finalString);
	}
	
	public static String reverseTheString(String str) {
		String reverse = "";
		char[] charArray = str.toCharArray();
		for (char a : charArray) {
			reverse = a + reverse;
		}
		return reverse;
	}

}
