package com.dsa;

public class FirstNonRepeatingChar {
    public static void main(String[] args) {
        firstNonRepeat("xxyyzz");
    }

    public static void firstNonRepeat(String str){
        int arr[] = new int[256];
        for (int i = 0; i < str.length(); i++) {
            arr[str.charAt(i)]++;
        }
        for (int i = 0; i < 256; i++) {
            if (arr[i] == 1) {
                System.out.println((char) (i));
                break;
            }
        }
    }
}
