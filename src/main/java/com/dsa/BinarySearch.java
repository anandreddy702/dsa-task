package com.dsa;

public class BinarySearch {

    public static void main(String[] args) {
        int arr[] = {2, 3, 4, 10, 40, 50};
        System.out.println(binarySearch(arr, 0, 4, 3));
    }
    public static int binarySearch(int arr[], int left, int right, int value) {
        if (right >= left) {
            int mid = left + (right - 1) / 2;
            if (value == arr[mid])
                return mid;
            if (arr[mid] > value)
                return binarySearch(arr, left, mid - 1, value);
            return binarySearch(arr, mid + 1, right, value);
        }
        return -1;
    }
}
